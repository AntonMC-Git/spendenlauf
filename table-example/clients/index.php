<?php

error_reporting(-1);
ini_set('display_errors', 'On');

include "../models/ClientRepository.php";

$config = include("../db/config-table.php");
$db = new PDO($config["db"], $config["username"], $config["password"]);
$clients = new ClientRepository($db);


switch($_SERVER["REQUEST_METHOD"]) {
    case "GET":
        $result = $clients->getAll(array(
            "firstName" => $_GET["firstName"],
            "lastName" => $_GET["lastName"],
            "class" => $_GET["class"]
        ));
        break;

    case "POST":
        $result = $clients->insert(array(
            "code" => $_POST["code"],
            "firstName" => $_POST["firstName"],
            "lastName" => $_POST["lastName"],
            "class" => $_POST["class"],
            "roundCount" => $_POST["roundCount"]
        ));
        break;

    case "PUT":
        parse_str(file_get_contents("php://input"), $_PUT);

        $result = $clients->update(array(
            "code" => intval($_PUT["code"]),
            "firstName" => $_PUT["firstName"],
            "lastName" => $_PUT["lastName"],
            "class" => $_PUT["class"],
            "roundCount" => $_PUT["roundCount"]
        ));
        break;

    case "DELETE":
        parse_str(file_get_contents("php://input"), $_DELETE);

        $result = $clients->remove(intval($_DELETE["code"]));
        break;
}


header("Content-Type: application/json");
echo json_encode($result);

?>
