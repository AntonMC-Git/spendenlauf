$(function() {

    $("#jsGrid").jsGrid({
        height: "100%",
        width: "100%",
        filtering: true,
        inserting: true,
        editing: true,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 5,
        deleteConfirm: "Schueler loeschen",
        controller: {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "../table-example/clients/",
                    data: filter
                });
            },
            insertItem: function(item) {
                return $.ajax({
                    type: "POST",
                    url: "../table-example/clients/",
                    data: item
                });
            },
            updateItem: function(item) {
                return $.ajax({
                    type: "PUT",
                    url: "../table-example/clients/",
                    data: item
                });
            },
            deleteItem: function(item) {
                return $.ajax({
                    type: "DELETE",
                    url: "../table-example/clients/",
                    data: item
                });
            }
        },
        fields: [
            { name: "code", title: "Code", type: "number", width: 50, filtering: false },
            { name: "firstName", title: "Vorname", type: "text", width: 150, filtering: true },
            { name: "lastName", title: "Nachname", type: "text", width: 150, filtering: true },
            { name: "class", title: "Klasse", type: "text", width: 150, filtering: true },
            { name: "roundCount", title: "Rundenanzahl", type: "number", width: 50, filtering: false },
            { type: "control" }
        ]
    });

});