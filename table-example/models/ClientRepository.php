<?php

include "Client.php";

class ClientRepository {

    protected $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }

    private function read($row) {
        $result = new Client();
        $result->code = $row["code"];
        $result->firstName = $row["firstName"];
        $result->lastName = $row["lastName"];
        $result->class = $row["class"];
        $result->roundCount = $row["roundCount"];
        return $result;
    }

    public function getById($id) {
        $sql = "SELECT * FROM rundenanzahl WHERE code = :code";
        $q = $this->db->prepare($sql);
        $q->bindParam(":code", $id, PDO::PARAM_INT);
        $q->execute();
        $rows = $q->fetchAll();
        return $this->read($rows[0]);
    }

    public function getAll($filter) {
        $firstName = "%" . $filter["firstName"] . "%";
        $lastName = "%" . $filter["lastName"] . "%";
        $class = "%" . $filter["class"] . "%";

        $sql = "SELECT * FROM rundenanzahl WHERE firstName LIKE :firstName AND lastName LIKE :lastName AND class LIKE :class";
        $q = $this->db->prepare($sql);
        $q->bindParam(":firstName", $firstName);
        $q->bindParam(":lastName", $lastName);
        $q->bindParam(":class", $class);
        $q->execute();
        $rows = $q->fetchAll();

        $result = array();
        foreach($rows as $row) {
            array_push($result, $this->read($row));
        }
        return $result;
    }
    
    public function insert($data) {
        $sql = "INSERT INTO rundenanzahl (code, firstName, lastName, class, roundCount) VALUES (:code, :firstName, :lastName, :class, :roundCount)";
        $q = $this->db->prepare($sql);
        $q->bindParam(":code", intval($data["code"]), PDO::PARAM_INT);
        $q->bindParam(":firstName", $data["firstName"]);
        $q->bindParam(":lastName", $data["lastName"]);
        $q->bindParam(":class", $data["class"]);
        $q->bindParam(":roundCount", intval($data["roundCount"]), PDO::PARAM_INT);
        $q->execute();
        return $this->getById($data["code"]); //FIXME: Ohne Code Eingabe wird der eingefügte Name nicht angezeigt
    }

    public function update($data) {
        $sql = "UPDATE rundenanzahl SET firstName = :firstName, lastName = :lastName, class = :class, roundCount = :roundCount WHERE code = :code";
        $q = $this->db->prepare($sql);
        $q->bindParam(":firstName", $data["firstName"]);
        $q->bindParam(":lastName", $data["lastName"]);
        $q->bindParam(":class", $data["class"]);
        $q->bindParam(":roundCount", $data["roundCount"], PDO::PARAM_INT);
        $q->bindParam(":code", $data["code"], PDO::PARAM_INT);
        $q->execute();
    }

    public function remove($id) {
        $sql = "DELETE FROM rundenanzahl WHERE code = :id";
        $q = $this->db->prepare($sql);
        $q->bindParam(":id", $id, PDO::PARAM_INT);
        $q->execute();
    }

}

?>