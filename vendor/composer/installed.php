<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '4cadb2727e7c8e2d89451cd98c6b0f57a1e2e11d',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '4cadb2727e7c8e2d89451cd98c6b0f57a1e2e11d',
            'dev_requirement' => false,
        ),
        'robthree/twofactorauth' => array(
            'pretty_version' => '1.8.0',
            'version' => '1.8.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../robthree/twofactorauth',
            'aliases' => array(),
            'reference' => '30a38627ae1e7c9399dae67e265063cd6ec5276c',
            'dev_requirement' => false,
        ),
    ),
);
