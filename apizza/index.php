<?php 
include 'pdo.php';
date_default_timezone_set('Germany/Berlin');



//Function tests TODO: Remove them after finishing backend work
/*echo(arrayToString(codeOwnerInformations("420000")));
echo("<br>");
echo(arrayToString(addRound("420000")));
echo("<br>");
echo(getTotalRoundCount());
echo($_POST["code"]);*/

//if(!isset($_POST["code"]))die("Diese Seite lässt nur direkte API Anfragen zu");

if(isset($_POST["function"])){
    if($_POST["function"] == "codeOwnerInformations"){
        if(isset($_POST["code"])){
            //echo(arrayToString(codeOwnerInformations("420000")));
            //$result = "{'test':'test3'}";
            /*$result = array(
                "doesOwnerExist" => true,
                "firstName" => "MeinVorname",
                "lastName" => "MeinNachname",
                "roundCount" => "13",
                "error" => ""
            );*/
            $result = codeOwnerInformations($_POST["code"]);
            header("Content-Type: application/json");
            echo json_encode($result);
        } else {
            //echo("Test");
            //echo("You got debugged");
            //echo($_POST["code"]);
            //$result = "{'test':'test2'}";
            header("Content-Type: application/json");
            echo json_encode($result);
        }
    } else if($_POST["function"] == "addRound"){
        /*$result = array(
            "success" => true,
            "roundCount" => 14,
            "error" => ""
        );*/
        $result = addRound($_POST["code"]);
        header("Content-Type: application/json");
        echo json_encode($result);
    } else if($_POST["function"] == "getDashboardInfo"){
        $result = array(
            "success" => true,
            "error" => "",

            "personCount" => getParticipantCount(),

            "totalRoundCount" => getTotalRoundCount(),
            "totalClassCount" => 43,
            "roundsPerHour" => getRounndsPerHour(),
            "participantCount" => getParticipantCount(),

            "timeSinceEventStart" => 12344,

            "year5" => 369,
            "year6" => 234,
            "year7" => 245,
            "year8" => 269,
            "year9" => 299,
            "year10" => 313,
            "year11" => 333,

            "year5average" => 3.7,
            "year6average" => 2.34,
            "year7average" => 2.45,
            "year8average" => 2.69,
            "year9average" => 2.99,
            "year10average" => 3.13,
            "year11average" => 3.33
            

        );
        header("Content-Type: application/json");
        echo json_encode($result);
    }
}

//Get Code Owner Informations - Return [doesOwnerExist: true/false, firstName: String, lastName: String, roundCount: INT, error: STRING]
function codeOwnerInformations($code){
    //Create a general database connection for later use
    $pdo = new PDO('mysql:host=localhost;dbname=spendenlauf;charset=utf8', $GLOBALS['USERNAME'], $GLOBALS['PASSWORT']); 

    //Create empty error String for later use in case of errors
    $error = "";

    //Verify code validity (Regex)
    if(!checkCodeValidity($code)){
        return(returnit("false","","","","Wrong code format"));
    }

    //Search for Code in Database
    $sql = "SELECT * FROM rundenanzahl WHERE code = " . $code;
    foreach ($pdo->query($sql) as $row) {
        //Debugging relevant Informations for testing purposes
        //echo $row['firstName']." ".$row['lastName']."<br />";
        //echo "Klasse: ".$row['class']."<br /><br />";

        //Set Variables according to data from database
        $doesOwnerExist = true;
        $firstName = $row['firstName'];
        $lastName = $row['lastName'];
        $roundCount = $row['roundCount'];
    }

    //If there's an error geeting the person throw it (out of the window)
    if(!isset($doesOwnerExist)||!isset($firstName)||!isset($lastName)||!isset($roundCount)){
        return(returnit(false,"","","","Person not found"));
    }

    //Return person details for further processing
    return(returnit($doesOwnerExist, $firstName, $lastName, $roundCount, $error));

}

function returnit($doesOwnerExistret, $firstNameret, $lastNameret, $roundCountret, $errorret){
    return([
        "doesOwnerExist" => $doesOwnerExistret,
        "firstName" => $firstNameret,
        "lastName" => $lastNameret,
        "roundCount" => $roundCountret,
        "error" => $errorret
    ]); //Array zurückgeben
}

//Add Round to user
function addRound($code){
    //Take user
    //Verify code validity (Regex)
    //Search for user in db
    //Add one round
    //Add one round to general round count and update general round count frontend (AJAX?)
    //Return successmesage and new roundcount

    //Create a general database connection for later use
    $pdo = new PDO('mysql:host=localhost;dbname=spendenlauf;charset=utf8', $GLOBALS['USERNAME'], $GLOBALS['PASSWORT']); 

    //Create empty error String for later use in case of errors
    $error = "";
    
    //Check code validity
    if(!checkCodeValidity($code)){
        return(returnAddRound(false,"","Wrong code format: " + $code));
    }

    //Search for Code in Database
    $sql = "SELECT * FROM rundenanzahl WHERE code = " . $code;
    foreach ($pdo->query($sql) as $row) {
        //Debugging relevant Informations for testing purposes
        //echo $row['firstName']." ".$row['lastName']."<br />";
        //echo "Klasse: ".$row['class']."<br /><br />";

        //Set Variables according to data from database
        $roundCount = $row['roundCount'] + 1;

        $statement = $pdo->prepare("UPDATE rundenanzahl SET roundCount = ? WHERE code = ?");
        $statement->execute(array($roundCount, $code));
    }

    //If there's an error geeting the person throw it (out of the window)
    if(!isset($roundCount)){
        return(returnAddRound(false,"","Person not found"));
    }

    //Return person details for further processing
    return(returnAddRound(true, $roundCount, $error));

}

function returnAddRound($successret, $roundCountret, $errorret){
    return([
        "success" => $successret,
        "roundCount" => $roundCountret,
        "error" => $errorret
    ]); //Array zurückgeben
}

//Utility stuff
function checkCodeValidity($code){
    $str = $code;
    $pattern = "/^\d{6}$/"; //TODO: RegEx Pattern anpasen
    if(!preg_match($pattern, $str)){
        return(false);
    }

    return(true);
}

function arrayToString($array){
    $arr = $array;
    $str = '';
    $glue = ', ';
    $cb = function($value, $key) use (&$str, $glue) {
        $str .= "$key: $value$glue";
    };
    array_walk_recursive($arr, $cb);
    $str = ($str!=='') ? mb_substr($str, 0, -mb_strlen($glue)) : $str; // letztes Komma wird wieder entfernt
    var_dump($str);
    return($str);
}

//Roundoverview (Show every person, their round count and have a uodate utility)
function getRoundOverview(){
    //Establish DB Connection
    //Get all users
    //Insert Data into the table
}

//General Round Counter (Return the amount of rounds run in total)
function getTotalRoundCount(){

    //Create a general database connection for later use
    $pdo = new PDO('mysql:host=localhost;dbname=spendenlauf;charset=utf8', $GLOBALS['USERNAME'], $GLOBALS['PASSWORT']); 

    //Get Total Round count
    $sql = "SELECT SUM(roundCount) FROM rundenanzahl";
    $user = $pdo->query($sql)->fetch();

    //Return total Round Count
    return($user["0"]);
}

function getParticipantCount(){
    //Create a general database connection for later use
    $pdo = new PDO('mysql:host=localhost;dbname=spendenlauf;charset=utf8', $GLOBALS['USERNAME'], $GLOBALS['PASSWORT']); 

    //Get data
    $statement = $pdo->prepare("SELECT * FROM rundenanzahl");
    $statement->execute(); 

    //Return Participant Count
    return($statement->rowCount());
}

function getRounndsPerHour(){
    //Create a general database connection for later use
    $pdo = new PDO('mysql:host=localhost;dbname=spendenlauf;charset=utf8', $GLOBALS['USERNAME'], $GLOBALS['PASSWORT']); 

    //Get Total Round count
    $sql = "SELECT SUM(roundCount) FROM rundenanzahl";
    $user = $pdo->query($sql)->fetch();
    $totalRoundCount = $user["0"];

    //Divide by hours since start
    $time_stamp = strtotime("2022-05-25 09:00:00");
    $time_difference = strtotime('now') - $time_stamp;
    $time_difference_hours = $time_difference / (60.0*60); 

    //Return total Round Count
    return(round($user["0"] / $time_difference_hours,2));

    //KK
    echo("Hmm");
}

?>