<?php
  session_start();
  if(!isset($_SESSION["login"])){
    header("Location: login.php");
    exit();
  }

?>
<html>
    <head>
        <title>Spendenlaufverifizierung | Gymnasium Vegesack</title>
        <link rel="stylesheet" href="style.css">
        <meta charset="utf-8"/>
        <link rel = "icon" href = "favicon.ico" type = "image/x-icon">

    </head>

    <body>
        <div class="container">
            <form id="form">
              <h1>SPENDENLAUF RUNDENCOUNTER</h1>
              <div class="form__group form__pincode">
                <label>Bitte den jeweiligen Schüler*Innen-Code eingeben.</label>
                <input type="tel" name="pincode-1" maxlength="1" pattern="[\d]*" tabindex="1" placeholder="·" autocomplete="off" autofocus>
                <input type="tel" name="pincode-2" maxlength="1" pattern="[\d]*" tabindex="2" placeholder="·" autocomplete="off">
                <input type="tel" name="pincode-3" maxlength="1" pattern="[\d]*" tabindex="3" placeholder="·" autocomplete="off">
                <input type="tel" name="pincode-4" maxlength="1" pattern="[\d]*" tabindex="4" placeholder="·" autocomplete="off">
                <input type="tel" name="pincode-5" maxlength="1" pattern="[\d]*" tabindex="5" placeholder="·" autocomplete="off">
                <input type="tel" name="pincode-6" maxlength="1" pattern="[\d]*" tabindex="6" placeholder="·" autocomplete="off">
              </div>

              <!--<video id="preview" style="width: 100%; margin-bottom: 15px;"></video>-->

              <div style="visibility: hidden; text-align: center" id="studentinfo">
                <p style="padding-bottom: 20px; padding-top: 5px;" id="studentname">ICh bin dumm</p>
                <p style="font-size:xx-large;" id="studentroundcount">42</p>
              </div>

              <div class="form__buttons">
                <a href="#" class="button button--primary" disabled>Runde hinzufügen</a>
                <a href="#" class="button button--reset">Feld leeren</a>
              </div>
            </form>
        </div>

       <script
            src="https://code.jquery.com/jquery-3.6.0.slim.min.js"
            integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI="
            crossorigin="anonymous"></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/jquery.inputmask.min.js'></script>
        <script src='https://cdn.jsdelivr.net/npm/jquery-mockjax@2.2.2/src/jquery.mockjax.min.js'></script>
        <script src="code.js"></script>

        <!--<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
        <script>
          let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
          scanner.addListener('scan', function (content) {
            alert(content);
          });
          Instascan.Camera.getCameras().then(function (cameras) {
            if (cameras.length > 0) {
              scanner.start(cameras[0]);
            } else {
              console.error('No cameras found.');
            }
          }).catch(function (e) {
            console.error(e);
          });
        </script>-->
          
    </body>
</html>
