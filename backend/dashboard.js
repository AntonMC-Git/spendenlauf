$(function() {

_req = $.ajax({
    type: 'POST',
    //url: '/api/tfa',
    //Return "ok": true/false
    url: '../apizza/',
    data: {
        'function': 'getDashboardInfo',
        '_csrf': ''
    },
    dataType : "json",
})
.done(function(data2, textStatus2, jqXHR2) {
    try {
        //Look whether it worked and then show an information banner
        if(data2.success){
            //Update total round count
            //console.log("Total Round Count" + data2.totalRoundCount);
            //$('#totalRoundCount').text(data2.totalRoundCount);
        } else {
            console.log("Error: " + data2.error);
        }
    } catch (err) {
        console.log(err);
    }
})
.fail(function(jqXHR, textStatus, errorThrown) {
    //Show error message
    console.log("ERROR" + errorThrown + jqXHR + textStatus);
})

setInterval(function() { 
    // alle 3 Sekunden ausführen 
    _req = $.ajax({
        type: 'POST',
        //url: '/api/tfa',
        //Return "ok": true/false
        url: '../apizza/',
        data: {
            'function': 'getDashboardInfo',
            '_csrf': ''
        },
        dataType : "json",
    })
    .done(function(data2, textStatus2, jqXHR2) {
        try {
            //Look whether it worked and then show an information banner
            if(data2.success){
                //Update total round count
                console.log("Total Round Count" + data2.totalRoundCount);
                $('#totalRoundCount').text(data2.totalRoundCount);

                //Update class count
                $('#classCount').text(data2.totalClassCount);

                //Update Rounds per Hour
                $('#roundsPerHour').text(data2.roundsPerHour);

                //Update Total Person Count
                $('#participantCount').text(data2.personCount);
            } else {
                console.log("Error: " + data2.error);
            }
        } catch (err) {
            console.log(err);
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        //Show error message
        console.log("ERROR" + errorThrown + jqXHR + textStatus);
    })
}, 1000);

});

//TODO: Loading spinner on the four things