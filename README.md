# README #

Das hier ist die git-Seite des Verifizierungsprogramms zum Spendenlauf am [Gymnasium Vegesack](https://www.gymnasium-vegesack-bremen.de/index.php/de/).

### To Do ###

* Backend
    * App-Kommunikation (L)
    * Datenbank (L)
    * Urkunden (M)
* Frontend
    * css, html (B)
* Datenkonvertierung(ausstehend) (M)

#### Urkunden ####

* Name, Code, Runden, Geschwindigkeit(?)
* Option 1: Ausgedruckt
    * depends: Live Daten zu Code anzeigen lassen
* Option 2: Serienbrief

### Kontakt ###

Bei jeglichem Interesse an Kontakt ist eine E-Mail an mrybka (at) tutanota (dot) com zu senden.
Alternativ kann ein oeffentlich sichtbares git Issue erstellt werden.